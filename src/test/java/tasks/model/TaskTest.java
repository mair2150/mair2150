package tasks.model;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

class TaskTest {
    private SimpleDateFormat sdf;
    private SimpleDateFormat sdfWorsePrecision;
    private String validTitle;
    private Date validStart, validEnd;
    private int validInterval;
    private Task nonRepeatedTask;
    private Task repeatedTask;

    @BeforeEach
    void setUp() {
        sdf = Task.getDateFormat2();
        sdfWorsePrecision = Task.getDateFormat();
        validTitle = "some task name";
        validInterval = 1;
        try {
            validStart = sdf.parse("2020-03-26 8:00:00.000");
            validEnd = sdf.parse("2020-03-26 10:00:00.000");

            nonRepeatedTask = new Task(validTitle, validStart);
            repeatedTask = new Task(validTitle, validStart, validEnd, validInterval);
            nonRepeatedTask.setActive(true);
            repeatedTask.setActive(true);
        } catch (ParseException e) {
            fail(e.getMessage());
        }
    }

    @AfterEach
    void tearDown() {
    }

//    F01. TC01
    @Test
    public void createTask_valid() {
        try {
            final Date start = sdf.parse("1970-01-01 00:00:00.001");
            assertEquals(1, start.getTime());
            Task task = new Task(validTitle, start, validEnd, validInterval);

            assert task != null;
            assertEquals(validTitle, task.getTitle());
            assertEquals(start, task.getStartTime());
            assertEquals(validEnd, task.getEndTime());
            assertEquals(validInterval, task.getRepeatInterval());
        } catch (ParseException e) {
            fail(e.getMessage());
        }
    }

//    F01. TC02
    @Test
    public void createTask_intervalEqualsZero() {
        final int interval = 0;
        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            Task task = new Task(validTitle, validStart, validEnd, interval);
        });

        final String actualMessage = exception.getMessage();
        final String expectedMessage = "interval should be >= 1";

        assertEquals(expectedMessage, actualMessage);
    }

//    F01. T03
    @Test
    public void createTask_startGetTimeEqualsMinusOne() {
        try {
            final Date start = sdf.parse("1969-12-31 23:59:59.999");
            assertEquals(start.getTime(), -1);
            Exception exception = assertThrows(IllegalArgumentException.class, () -> {
                Task task = new Task(validTitle, start, validEnd, validInterval);
            });

            final String actualMessage = exception.getMessage();
            final String expectedMessage = "Time cannot be negative";

            assertEquals(expectedMessage, actualMessage);
        } catch (ParseException e) {
            fail(e.getMessage());
        }
    }

//    F01. T04
    @Test
    public void createTask_startGetTimeEqualsZero() {
        try {
            final Date start = sdf.parse("1970-01-01 00:00:00.000");
            assertEquals(start.getTime(), 0);
            Task task = new Task(validTitle, start, validEnd, validInterval);

            assert task != null;
            assertEquals(validTitle, task.getTitle());
            assertEquals(start, task.getStartTime());
            assertEquals(validEnd, task.getEndTime());
            assertEquals(validInterval, task.getRepeatInterval());
        } catch (ParseException e) {
            fail(e.getMessage());
        }
    }

//    F01. T05
    @Test
    public void createTask_startGetTimeAndIntervalEqualMaxMinusOne() {
        final Date start = new Date(Long.MAX_VALUE - 1);
        final int interval = Integer.MAX_VALUE - 1;
        Task task = new Task(validTitle, start, validEnd, interval);

        assert task != null;
        assertEquals(validTitle, task.getTitle());
        assertEquals(start, task.getStartTime());
        assertEquals(validEnd, task.getEndTime());
        assertEquals(interval, task.getRepeatInterval());
    }

//    F01. T06
    @Test
    public void createTask_startGetTimeAndIntervalEqualMax() {
        final Date start = new Date(Long.MAX_VALUE);
        final int interval = Integer.MAX_VALUE;
        Task task = new Task(validTitle, start, validEnd, interval);

        assert task != null;
        assertEquals(validTitle, task.getTitle());
        assertEquals(start, task.getStartTime());
        assertEquals(validEnd, task.getEndTime());
        assertEquals(interval, task.getRepeatInterval());
    }

//    F01. T07
    @Test
    public void createTask_intervalEqualsMaxPlusOne() {
        final int interval = Integer.MAX_VALUE + 1;
        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            Task task = new Task(validTitle, validStart, validEnd, interval);
        });

        final String actualMessage = exception.getMessage();
        final String expectedMessage = "interval should be >= 1";

        assertEquals(expectedMessage, actualMessage);
    }

//    F01. T08
    @Test
    public void createTask_startGetTimeEqualsMaxPlusOne() {
        final Date invalidStart = new Date(Long.MAX_VALUE + 1);
        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            Task task = new Task(validTitle, invalidStart, validEnd, validInterval);
        });

        final String actualMessage = exception.getMessage();
        final String expectedMessage = "Time cannot be negative";

        assertEquals(actualMessage, expectedMessage);
    }

////    F02. T0
//    @Test
//    public void nextTimeAfter_nonRepeatedTaskBeforeStart() {
//        try {
//            final Date beforeStart = sdf.parse("2020-03-26 7:00:00.000");
//            assert(beforeStart.before(validStart));
//
//            final Date result = nonRepeatedTask.nextTimeAfter(beforeStart);
//            assertEquals(validStart, result);
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//    }
//
////    F02. T0
//    @Test
//    public void nextTimeAfter_repeatedTaskBeforeStart() {
//
//        try {
//            final Date beforeStart = sdf.parse("2020-03-26 7:00:00.000");
//            assert(beforeStart.before(validStart));
//
//            final Date result = repeatedTask.nextTimeAfter(beforeStart);
//            assertEquals(validStart, result);
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//    }

////    F02. T0
//    @Test
//    public void nextTimeAfter_repeatedTaskEqualToStart() {
//        final Date result = repeatedTask.nextTimeAfter(validStart);
//
//        try {
//            final Date expectedDate = sdf.parse("2020-03-26 8:00:01.000");
//            assertEquals(expectedDate, result);
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//    }
//
////    F02. T0
//    @Test
//    public void nextTimeAfter_repeatedTaskBetweenStartAndEnd() {
//        try {
//            final Date betweenStartAndEnd = sdf.parse("2020-03-26 9:00:00.000");
//            assert(betweenStartAndEnd.after(validStart));
//            assert(betweenStartAndEnd.before(validEnd));
//
//            final Date result = repeatedTask.nextTimeAfter(betweenStartAndEnd);
//
//            final Date expectedDate = sdf.parse("2020-03-26 9:00:01.000");
//            assertEquals(expectedDate, result);
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//    }
//
////    F02. T0
//    @Test
//    public void nextTimeAfter_currentEqualsToEnd() {
//        final Date result = nonRepeatedTask.nextTimeAfter(validEnd);
//        assertEquals(null, result);
//    }
//
////    F02. T0
//    @Test
//    public void nextTimeAfter_notActiveTask() {
//        nonRepeatedTask.setActive(false);
//        final Date result = nonRepeatedTask.nextTimeAfter(validStart);
//        assertEquals(null, result);
//    }
}