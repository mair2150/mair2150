package tasks.model;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class TaskListTest {
    private ArrayTaskList arrayTaskList;
    private TaskList linkedTaskList;
    private String taskName;

    private SimpleDateFormat sdf;
    private Date validFrom;
    private Date validBeforeFrom;
    private Date validTo;
    private Date validAfterTo;

    @BeforeEach
    void setUp() {
        sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        try {
            validFrom = sdf.parse("2020-03-26 10:00");
            validBeforeFrom = sdf.parse("2020-03-25 10:00");
            validTo = sdf.parse("2020-03-27 10:00");
            validAfterTo = sdf.parse("2020-03-28 10:00");
        } catch (ParseException e) {
            fail(e.getMessage());
        }

        arrayTaskList = new ArrayTaskList();
        linkedTaskList = new LinkedTaskList();

        taskName = "some task name";
    }

    @AfterEach
    void tearDown() {
    }

    //    F02. TC01
    @Test
    void incoming_fromIsNull() {
        final TaskList result = arrayTaskList.incoming(null, validTo);
        assertNull(result);

    }

    //    F02. TC02
    @Test
    void incoming_toIsNull() {
        final TaskList result = arrayTaskList.incoming(validFrom, null);
        assertNull(result);
    }

    //    F02. TC03
    @Test
    void incoming_taskBetweenFromAndTo() {
        final Task task = new Task(taskName, validFrom, validTo, 60);
        task.setActive(true);
        arrayTaskList.add(task);
        final TaskList result = arrayTaskList.incoming(validBeforeFrom, validAfterTo);

        assertNotNull(result);
        assertTrue(result instanceof ArrayTaskList);
        assertEquals(1, result.size());

        final Task resultTask = result.getTask(0);
        assertEquals(taskName, resultTask.getTitle());
    }

    //    F02. TC04
    @Test
    void incoming_taskBeforeFrom() {
        final Task task = new Task(taskName, validFrom, validTo, 60);
        task.setActive(true);
        linkedTaskList.add(task);

        try {
            final Date from = sdf.parse("2020-03-28 10:00");
            final Date to = sdf.parse("2020-03-29 10:00");
            final TaskList result = linkedTaskList.incoming(from, to);

            assertNotNull(result);
            assertEquals(0, result.size());
            assertTrue(result instanceof LinkedTaskList);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    //    F02. TC05
    @Test
    void incoming_taskAfterTo() {
        final Task task = new Task(taskName, validFrom, validTo, 60);
        task.setActive(true);
        linkedTaskList.add(task);
        try {
//        validFrom + interval > almostFrom
            final Date from = sdf.parse("2020-03-26 22:0");
            final Date to = sdf.parse("2020-03-26 22:01");
            final TaskList result = linkedTaskList.incoming(from, to);

            assertNotNull(result);
            assertEquals(0, result.size());
            assertTrue(result instanceof LinkedTaskList);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

//    Testare unitara
    @Test
    void incoming_mock() {
        Task taskMock = mock(Task.class);
        Mockito.when(taskMock.getTitle()).thenReturn(taskName);
        Mockito.when(taskMock.nextTimeAfter(validBeforeFrom)).thenReturn(validTo);

        arrayTaskList.add(taskMock);
        final TaskList result = arrayTaskList.incoming(validBeforeFrom, validAfterTo);

        Mockito.verify(taskMock, times(0)).getTitle();
        Mockito.verify(taskMock, times(2)).nextTimeAfter(validBeforeFrom);

        assertNotNull(result);
        assertTrue(result instanceof ArrayTaskList);
        assertEquals(1, result.size());

        final Task resultTask = result.getTask(0);
        assertEquals(taskName, resultTask.getTitle());

        Mockito.verify(taskMock, times(1)).getTitle();
    }

//    Testare unitara
    @Test
    void incoming_spy() {
        final Task task = new Task(null, validBeforeFrom, validBeforeFrom, 60);
        Task taskSpy = spy(task);
        Mockito.when(taskSpy.nextTimeAfter(validFrom)).thenReturn(null);

        arrayTaskList.add(taskSpy);
        final TaskList result = arrayTaskList.incoming(validBeforeFrom, validAfterTo);

        Mockito.verify(taskSpy, times(1)).nextTimeAfter(validBeforeFrom);

        assertNotNull(result);
        assertTrue(result instanceof ArrayTaskList);
        assertEquals(0, result.size());
    }
}
