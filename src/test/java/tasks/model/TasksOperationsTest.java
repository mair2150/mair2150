package tasks.model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.internal.util.collections.Iterables;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class TasksOperationsTest {
    private SimpleDateFormat sdf;
    private Date validBeforeStart;
    private Date validStart;
    private Date validEnd;
    private Date validAfterEnd;

    private String taskName;

    @BeforeEach
    void setUp() {
        sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        try {
            validBeforeStart = sdf.parse("2020-03-25 10:00");
            validStart = sdf.parse("2020-03-26 10:00");
            validEnd = sdf.parse("2020-03-27 10:00");
            validAfterEnd = sdf.parse("2020-03-28 10:00");
        } catch (ParseException e) {
            fail(e.getMessage());
        }

        taskName = "fake task name";
    }

    @AfterEach
    void tearDown() {
    }

//    Testare unitara
    @Test
    void incoming_mock() {
        ObservableList<Task> tasksListMock = mock(ObservableList.class);

        Task task = new Task(taskName, validStart, validEnd, 60);
        task.setActive(true);
        Object[] tasks = {task};
        Mockito.when(tasksListMock.toArray()).thenReturn(tasks);

        TasksOperations tasksOperations = new TasksOperations(tasksListMock);
        final Iterable<Task> result = tasksOperations.incoming(validBeforeStart, validAfterEnd);

        Mockito.verify(tasksListMock, times(1)).toArray();

        assertNotNull(result);

        Integer count = 0;
        for (Task x : result) { ++count; }
        assertEquals(1, count);

        final Task resultTask = result.iterator().next();
        assertEquals(taskName, resultTask.getTitle());
        assertEquals(validStart, resultTask.getStartTime());
        assertEquals(validEnd, resultTask.getEndTime());
    }

//    Testare unitara
    @Test
    void incoming_spy() {
        ObservableList<Task> tasksList =  FXCollections.observableArrayList();
        ObservableList<Task> tasksListSpy = spy(tasksList);

        Task task = new Task(taskName, validStart, validEnd, 60);
        task.setActive(true);
        Object[] tasks = {task};
        Mockito.when(tasksListSpy.toArray()).thenReturn(tasks);

        Mockito.verify(tasksListSpy, times(1)).toArray();

        TasksOperations tasksOperations = new TasksOperations(tasksListSpy);
        final Iterable<Task> result = tasksOperations.incoming(validBeforeStart, validAfterEnd);

        Mockito.verify(tasksListSpy, times(2)).toArray();

        assertNotNull(result);

        Integer count = 0;
        for (Task x : result) { ++count; }
        assertEquals(1, count);

        final Task resultTask = result.iterator().next();
        assertEquals(taskName, resultTask.getTitle());
        assertEquals(validStart, resultTask.getStartTime());
        assertEquals(validEnd, resultTask.getEndTime());
    }

//    Pas 2. Integrare R (TaskList)
    @Test
    void incoming_2_mock() {
//        Mock Task
        Task taskMock = mock(Task.class);
        when(taskMock.getTitle()).thenReturn(taskName);
        when(taskMock.nextTimeAfter(validBeforeStart)).thenReturn(validStart);

        ObservableList<Task> tasksList = FXCollections.observableArrayList(taskMock);

        TasksOperations tasksOperations = new TasksOperations(tasksList);
        final Iterable<Task> result = tasksOperations.incoming(validBeforeStart, validAfterEnd);

        Mockito.verify(taskMock, times(1)).nextTimeAfter(validBeforeStart);

        assertNotNull(result);

        Integer count = 0;
        for (Task x : result) { ++count; }
        assertEquals(1, count);

        final Task resultTask = result.iterator().next();
        assertEquals(taskName, resultTask.getTitle());

        Mockito.verify(taskMock, times(1)).getTitle();
    }

    //    Pas 2. Integrare R (TaskList)
    @Test
    void incoming_2_spy() {
//        Mock Task
        Task task = new Task(null, validBeforeStart);
        Task taskSpy = spy(task);
        when(taskSpy.getTitle()).thenReturn(taskName);
        when(taskSpy.nextTimeAfter(validBeforeStart)).thenReturn(validStart);

        ObservableList<Task> tasksList = FXCollections.observableArrayList(taskSpy);

        TasksOperations tasksOperations = new TasksOperations(tasksList);
        final Iterable<Task> result = tasksOperations.incoming(validBeforeStart, validAfterEnd);

        Mockito.verify(taskSpy, times(1)).nextTimeAfter(validBeforeStart);

        assertNotNull(result);

        Integer count = 0;
        for (Task x : result) { ++count; }
        assertEquals(1, count);

        final Task resultTask = result.iterator().next();
        assertEquals(taskName, resultTask.getTitle());

        Mockito.verify(taskSpy, times(1)).getTitle();
    }
}