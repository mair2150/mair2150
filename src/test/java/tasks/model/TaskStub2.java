package tasks.model;

import java.util.Date;

public class TaskStub2 extends Task {
    public TaskStub2(String title, Date time) {
        super(title, time);
    }

    public TaskStub2(String title, Date start, Date end, int interval) {
        super(title, start, end, interval);
    }

    @Override
    public boolean isActive(){
        return true;
    }

    public Date nextTimeAfter(Date current){
        return null;
    }
}
