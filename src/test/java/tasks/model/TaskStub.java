package tasks.model;

import java.util.Date;

public class TaskStub extends Task {
    public TaskStub(String title, Date time) {
        super(title, time);
    }

    public TaskStub(String title, Date start, Date end, int interval) {
        super(title, start, end, interval);
    }

    @Override
    public String getTitle() {
        return "task stub title";
    }

    @Override
    public boolean isActive(){
        return true;
    }

    public Date nextTimeAfter(Date current){
        return current;
    }
}
