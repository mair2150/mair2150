package tasks.services;

import javafx.collections.ObservableList;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import tasks.model.*;
import tasks.view.Main;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Array;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class TasksServiceTest {
    private SimpleDateFormat sdf;
    private Date validBeforeStart;
    private Date validStart;
    private Date validEnd;
    private Date validAfterEnd;

    private String taskName;
    private String taskName2;

    @BeforeEach
    void setUp() {
        sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        try {
            validBeforeStart = sdf.parse("2020-03-25 10:00");
            validStart = sdf.parse("2020-03-26 10:00");
            validEnd = sdf.parse("2020-03-27 10:00");
            validAfterEnd = sdf.parse("2020-03-28 10:00");
        } catch (ParseException e) {
            fail(e.getMessage());
        }

        taskName = "fake task name";
        taskName2 = "fake task name 2";
    }

    @AfterEach
    void tearDown() {
    }

////    Testare unitara
//    @Test
//    void getObservableList_mock() {
//        Task task = new Task(taskName, validStart, validEnd, 60);
//        task.setActive(true);
//        List<Task> taskList = new ArrayList<Task>();
//        taskList.add(task);
//
//        ArrayTaskList arrayTaskList = mock(ArrayTaskList.class);
//        when(arrayTaskList.getAll()).thenReturn(taskList);
//
//        TasksService tasksService = new TasksService(arrayTaskList);
//        final ObservableList<Task> result = tasksService.getObservableList();
//
//        Mockito.verify(arrayTaskList, times(1)).getAll();
//
//        assertNotNull(result);
//        assertEquals(1, result.size());
//
//        final Task resultTask = result.get(0);
//        assertEquals(taskName, resultTask.getTitle());
//        assertEquals(validStart, resultTask.getStartTime());
//        assertEquals(validEnd, resultTask.getEndTime());
//    }

//    Testare unitara
//    @Test
//    void getObservableList_spy() {
//        Task task = new Task(taskName, validStart, validEnd, 60);
//        task.setActive(true);
//
//        Task secondTask = new Task(taskName2, validBeforeStart);
//
//        List<Task> taskList = new ArrayList<Task>();
//        taskList.add(task);
//        taskList.add(secondTask);
//
//        ArrayTaskList arrayTaskList = new ArrayTaskList();
//        ArrayTaskList arrayTaskListSpy = spy(arrayTaskList);
//        when(arrayTaskListSpy.getAll()).thenReturn(taskList);
//
//        TasksService tasksService = new TasksService(arrayTaskListSpy);
//        final ObservableList<Task> result = tasksService.getObservableList();
//
//        Mockito.verify(arrayTaskListSpy, times(1)).getAll();
//
//        assertNotNull(result);
//        assertEquals(2, result.size());
//
//        assertEquals(taskName, result.get(0).getTitle());
//        assertEquals(taskName2, result.get(1).getTitle());
//    }

    //    Testare unitara
    @Test
    void filterTasks_mock() {
        Task task = new Task(taskName, validStart, validEnd, 60);
        task.setActive(true);
        List<Task> taskList = new ArrayList<Task>();
        taskList.add(task);

        ArrayTaskList arrayTaskListMock = mock(ArrayTaskList.class);
        when(arrayTaskListMock.getAll()).thenReturn(taskList);

        TasksService tasksService = new TasksService(arrayTaskListMock);
        final Iterable<Task> result = tasksService.filterTasks(validBeforeStart, validAfterEnd);

        Mockito.verify(arrayTaskListMock, times(1)).getAll();

        assertNotNull(result);
        Integer count = 0;
        for (Task _ : result) { ++count; }
        assertEquals(1, count);

        final Task resultTask = result.iterator().next();
        assertEquals(taskName, resultTask.getTitle());
        assertEquals(validStart, resultTask.getStartTime());
        assertEquals(validEnd, resultTask.getEndTime());
    }

    @Test
    void filterTasks_spy() {
        Task task = new Task(taskName, validStart, validEnd, 60);

        ArrayTaskList arrayTaskList = new ArrayTaskList();
        arrayTaskList.add(task);

        ArrayTaskList arrayTaskListSpy = spy(arrayTaskList);
        when(arrayTaskListSpy.getAll()).thenReturn(new ArrayList<Task>());

        TasksService tasksService = new TasksService(arrayTaskListSpy);
        final Iterable<Task> result = tasksService.filterTasks(validBeforeStart, validAfterEnd);

        Mockito.verify(arrayTaskListSpy, times(1)).getAll();

        assertNotNull(result);
        Integer count = 0;
        for (Task _ : result) { ++count; }
        assertEquals(0, count);
    }

//// Pas 2
//    @Test
//    void filterTasks_mock_2() {
//    //        Mock task
//        Task taskMock = mock(Task.class);
//        when(taskMock.getTitle()).thenReturn(taskName);
//        when(taskMock.nextTimeAfter(validBeforeStart)).thenReturn(validStart);
//
//        ArrayTaskList arrayTaskList = new ArrayTaskList();
//        arrayTaskList.add(taskMock);
//
//        TasksService tasksService = new TasksService(arrayTaskList);
//        final Iterable<Task> result = tasksService.filterTasks(validBeforeStart, validAfterEnd);
//
//        Mockito.verify(taskMock, times(1)).nextTimeAfter(validBeforeStart);
//
//        assertNotNull(result);
//        Integer count = 0;
//        for (Task _ : result) { ++count; }
//        assertEquals(1, count);
//
//        final Task resultTask = result.iterator().next();
//        assertEquals(taskName, resultTask.getTitle());
//    }
//
//    // Part 2
//    @Test
//    void filterTasks_spy_2() {
////        Mock task
//        Task task = new Task(null, validAfterEnd);
//        Task taskSpy = spy(task);
//        when(taskSpy.getTitle()).thenReturn(taskName);
//        when(taskSpy.nextTimeAfter(validStart)).thenReturn(null);
//
//        Mockito.verify(taskSpy, times(1)).nextTimeAfter(validStart);
//
//        ArrayTaskList arrayTaskList = new ArrayTaskList();
//        arrayTaskList.add(taskSpy);
//
//        TasksService tasksService = new TasksService(arrayTaskList);
//        final Iterable<Task> result = tasksService.filterTasks(validStart, validAfterEnd);
//
//        Mockito.verify(taskSpy, times(2)).nextTimeAfter(validStart);
//
//        assertNotNull(result);
//        Integer count = 0;
//        for (Task _ : result) { ++count; }
//        assertEquals(0, count);
//    }

    // Pas 2
    @Test
    void filterTasks_mock_2() {
        //        Stubbed mocks for Task
        Task stubbedMockTask = new TaskStub(null, validAfterEnd);

        ArrayTaskList arrayTaskList = new ArrayTaskList();
        arrayTaskList.add(stubbedMockTask);

        TasksService tasksService = new TasksService(arrayTaskList);
        final Iterable<Task> result = tasksService.filterTasks(validBeforeStart, validEnd);

        assertNotNull(result);
        Integer count = 0;
        for (Task _ : result) { ++count; }
        assertEquals(1, count);

        final Task resultTask = result.iterator().next();
        assertEquals("task stub title", resultTask.getTitle());
    }

    @Test
    void filterTasks_spy_2() {
        //        Stubbed mocks for Task
        Task stubbedMockTask = new TaskStub2(null,validEnd);

        ArrayTaskList arrayTaskList = new ArrayTaskList();
        arrayTaskList.add(stubbedMockTask);

        TasksService tasksService = new TasksService(arrayTaskList);
        final Iterable<Task> result = tasksService.filterTasks(validStart, validAfterEnd);

        assertNotNull(result);
        Integer count = 0;
        for (Task _ : result) { ++count; }
        assertEquals(0, count);
    }

// Pas 3
    @Test
    void filterTasks_mock_3() {
        Task task = new Task(taskName, validStart, validEnd, 60);
        task.setActive(true);

        ArrayTaskList arrayTaskList = new ArrayTaskList();
        arrayTaskList.add(task);

        TasksService tasksService = new TasksService(arrayTaskList);
        final Iterable<Task> result = tasksService.filterTasks(validBeforeStart, validAfterEnd);

        assertNotNull(result);
        Integer count = 0;
        for (Task _ : result) { ++count; }
        assertEquals(1, count);

        final Task resultTask = result.iterator().next();
        assertEquals(taskName, resultTask.getTitle());
    }
    @Test

    void filterTasks_spy_3() {
        Task task = new Task(taskName, validAfterEnd, validAfterEnd, 60);
        task.setActive(true);

        ArrayTaskList arrayTaskList = new ArrayTaskList();
        arrayTaskList.add(task);

        TasksService tasksService = new TasksService(arrayTaskList);
        final Iterable<Task> result = tasksService.filterTasks(validStart, validEnd);

        assertNotNull(result);
        Integer count = 0;
        for (Task _ : result) { ++count; }
        assertEquals(0, count);
    }
}